package routes


import "github.com/gofiber/fiber/v2"



func Setup() {
  app := fiber.New(fiber.Config{
	
  })

  app.Get("/api/allnotes", GetAllNotes)
  app.Get("/api/note/:id", GetNote)
  app.Post("/api/note/:id", UpdateNote)
  app.Post("/api/note", CreateNote)
  app.Delete("/api/note/:id", DeleteNote)


  app.Listen(":3000")
}