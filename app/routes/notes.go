package routes

import (
	"strconv"
	"fmt"
	"github.com/gofiber/fiber/v2"
)

type Notes struct {
	Title 		string `json:"title"`
	Note 		string `json:"note"`
	DateCreated string	`json:"dateCreated"`
	DateUpdated string	`json:"dateUpdated"`
	Completed 	bool	`json:"completed"`
}

func GetAllNotes(c *fiber.Ctx) error {
	note := Notes {
		Title: "My first note",
		Note: "This is a note",
		DateCreated: "1/21/2022",
		Completed: false,

	}
	return c.JSON(note)
}

func GetNote(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.JSON(fiber.Map {
			"message": err.Error(),
		})
	}
	fmt.Printf("id: %d\n", id)
	// SQL query for the actual note

	note := Notes {
		Title: "Another Note",
		Note: "This is my second awesome note",
		DateCreated: "22/1/2022",
		Completed: false,
	}
	return c.JSON(note)
	
}

func UpdateNote(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.JSON(fiber.Map {
			"message": err.Error(),
		})
	}
	fmt.Printf("id: %d\n", id)

	// update in the DB the note
	return c.JSON(fiber.Map {
		"message": "Updated Successfully!",
	})
}

func CreateNote(c *fiber.Ctx) error {
	c.Accepts("application/json")
	var note Notes
	c.BodyParser(&note)

	// put into the DB

	fmt.Printf("Note: %s %s\n", note.Title, note.Note)

	return c.JSON(fiber.Map {
		"message": "Note created!",
	})
	
}

func DeleteNote(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.JSON(fiber.Map {
			"message": err.Error(),
		})
	}
	fmt.Printf("id: %d\n", id)

	// delete this note from our DB

	return c.JSON(fiber.Map {
		"message": "Note successfully deleted",
	})
}