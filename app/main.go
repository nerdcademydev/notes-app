package main

import "gitlab.com/nerdcademydev/notes-app/routes"


func main() {
	routes.Setup()
}